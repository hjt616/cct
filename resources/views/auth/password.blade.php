@extends('layouts.auth')

@section('title') {{ trans('auth.forgot') }} @endsection

@section('body-class') hold-transition login-page @endsection

@section('content')
    <style>
        *{
            padding: 0px;
            margin:0px;
        }

        html,body{
            width:100%;
            height: 100%;
            background-image: url(https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1578481370629&di=7549e9da3390e7e9892f8dd6ebbd858a&imgtype=0&src=http%3A%2F%2Fimg.pptjia.com%2Fimage%2F20190320%2F3a8d9c68a5f17ae592cdb50c9147359d.jpg);
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        .login-box {
            width:500px;
            margin-right: auto;
            margin-left: auto;
            position: relative;
            top:35%;
            margin-bottom: 0;
            background-color: rgba(0, 0, 0, 0.03);
            border-bottom: 1px solid rgba(0, 0, 0, 0.125);
        }

        .c_name {
            font-size: 84px;
            font-family: "宋体";
            margin-left: 2%;
        }
    </style>
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>{{ Setting::get('app_name', '') }}</b>忘记密码</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @include('public/message')
        <form action="{{ url('auth/forgot/email') }}" method="post">
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="{{ trans('auth.email') }}" name="email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.forgot-btn') }}</button>
                </div>
            </div>
        </form>
        <br>
        <a href="{{ url('auth/login') }}" class="pull-left">{{ trans('auth.login') }}</a>
        <a href="{{ url('auth/register') }}" class="pull-right">{{ trans('auth.register') }}</a>
        <br>
    </div>
</div>

<div class="c_name">
    云链后台管理系统
</div>
@endsection
