@extends('layouts.front.user')

@section('title')
    {{ trans('admin.menu_list.user') }}
@endsection
    <style>
        .box-body{
            width:1000px;
        }
        .box-footer{
            width:1000px;
        }
        .form-group{
            width:1000px;
        }

        .box{
            position: fixed;
            left: 10%;
        }

    </style>
@section('content')
    <!-- Main content -->
    <section class="content">
    @include('public/message')
    <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">资产发行配置</h3>
            </div>

            <div class="box-header with-border">
                @if(isset($item))
                    <h3 class="box-title">修改资产发行配置
                        <small>Coin Id: {{ $item->id }}</small>
                    </h3>
                @else
                    <h3 class="box-title">新增资产发行配置</h3>
                @endif
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            {{--action="{{ isset($item) ? url('admin/user/edit') :  url('admin/user/add') }}"--}}
            <form class="form-horizontal" action="{{ isset($item) ? url('coin/edit') :  url('coin/add') }}"  method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="capitalists" class="col-sm-2 control-label">资产方</label>
                        <div class="col-sm-10">
                            {{--<input type="text" class="form-control" id="username" value="{{ $item->username }}" readonly>--}}
                            <input type="text" class="form-control" id="capitalists" name="capitalists" value="{{ isset($item) ? $item->capitalists : old('capitalists') }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coin_type" class="col-sm-2 control-label">币种</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="coin_type" name="coin_type" value="{{ isset($item) ? $item->coin_type : old('coin_type') }}" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coin_num" class="col-sm-2 control-label">发币量</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="coin_num" name="coin_num" value="{{ isset($item) ? $item->coin_num : old('coin_num') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coin_unit" class="col-sm-2 control-label">单位</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="coin_unit" name="coin_unit" value="{{ isset($item) ? $item->coin_unit : old('coin_unit') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="coin_unit_min" class="col-sm-2 control-label">交割最小单位</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="coin_unit_min" name="coin_unit_min" value="{{ isset($item) ? $item->coin_unit_min : old('coin_unit_min') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">详情</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" name="description" value="{{ isset($item) ? $item->description : old('description') }}">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-cancel pull-left" onclick="location='{{ url('user') }}';">{{ trans('common.return') }}</button>
                    @if (isset($item))
                        <input type="hidden" name="id" value="{{ $item->id }}" />
                        <button type="submit" class="btn btn-info pull-right">{{ trans('common.edit') }}</button>
                    @else
                        <button type="submit" class="btn btn-success pull-right">{{ trans('common.add') }}</button>
                    @endif
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </section>

@endsection
@section('js')
    <script type="text/javascript" src="{{ Setting::get('cdn_url', '/') }}plugins/AjaxFileUpload/jquery.ajaxfileupload.js"></script>
    <script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/select2.full.min.js"></script>
    <script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/i18n/zh-CN.js"></script>
@endsection