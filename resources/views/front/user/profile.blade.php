@extends('layouts.front.user')
@section('title') 云链后台管理系统 | 管理员 @endsection

@section('css')
    <link rel="stylesheet" href="{{ Setting::get('cdn_url', '/') }}plugins/select2/select2.min.css">
@endsection

@section('content')

    <style>
        .box-body{
            width:1000px;
        }
        .box-footer{
            width:1000px;
        }
        .form-group{
            width:1000px;
        }
        .box{
            /*background: #E0E0E0;*/
            position: fixed;
            left: 10%;
        }
    </style>

    <!-- Main content -->
    <section class="content">
    @include('public/message')
    <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('user.menu_list.profile_edit') }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/user/profile') }}" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">{{ trans('common.username') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="username" value="{{ $item->username }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">{{ trans('common.email') }}</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email" value="{{ $item->email }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">{{ trans('common.description') }}</label>
                        <div class="col-sm-10">
                            <textarea id="description" title="description" name="description" class="form-control">{{ $item->profile->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">{{ trans('common.password') }}</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation"
                               class="col-sm-2 control-label">{{ trans('common.password_confirmation') }}</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                                   value="">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="button" class="btn btn-cancel pull-left" onclick="location='{{ url('user') }}';">{{ trans('common.return') }}</button>

                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-info pull-right">{{ trans('common.edit') }}</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </section>

@endsection

@section('js')
    <script type="text/javascript" src="{{ Setting::get('cdn_url', '/') }}plugins/AjaxFileUpload/jquery.ajaxfileupload.js"></script>
    <script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/select2.full.min.js"></script>
    <script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/i18n/zh-CN.js"></script>
    <script>
        $(function(){
            $('#avatar').AjaxFileUpload({
                action: $('#avatar').attr('data-url'),
                onComplete: function (filename, response) {
                    if (response.status == 'success') {
                        $('#avatar-img').attr('src', '/' + response.path + '?t=' + Math.random());
                        $('#avatar-input').val(response.path);
                    } else {
                        alert(response.msg);
                    }
                }
            });

            $('select').select2({
                placeholder: "{{ trans('common.role_select') }}",
                language: "zh-CN"
            });

            $('#province').change(function(){
                var $option = $(this).find("option:selected");
                getRegion('city',$option.val(),1,0);
            });

            $('#city').change(function() {
                var $option = $(this).find("option:selected");
                getRegion('area',$option.val(),2,0);
            });

            var provinceDefaultId = $('#province').find("option:selected").val();
            var cityDefaultId = {{ isset($item) ? (int)$item->profile->city : 0 }};
            var areaDefaultId = {{ isset($item) ? (int)$item->profile->area : 0}};
            if (provinceDefaultId > 0) {
                getRegion('city', provinceDefaultId, 1, cityDefaultId);
                if (cityDefaultId > 0) {
                    getRegion('area', cityDefaultId, 2, areaDefaultId);
                }
            }

        });

        function getRegion(_nextId, _pid, _type, _defalutId) {
            var $region = $('#'+_nextId);
            $region.html('<option value="0" selected>{{ trans('common.select') }}</option>');
            $.get('/user/region?type='+_type+'&pid='+_pid,function(data){
                if (data.status == 0) {
                    var _list = data.list;
                    if ($(_list).first().length != 0) {
                        $region.show();
                        $.each(_list, function(i,val){
                            $region.append('<option '+( val.id == _defalutId ? 'selected' : '' )+' value="'+val.id+'">'+val.name+'</option>');
                            $region.trigger('change.select2');
                        });
                    } else {
                        $region.hide();
                    }
                }
            });
        }
    </script>
@endsection
