@extends('layouts.front.user')

@if($rid == 4 )
    @section('title') 云链后台管理系统 | 操作员@endsection
@elseif($rid == 3)
    @section('title') 云链后台管理系统 | 审核员@endsection
@endif

@section('content')

    <section>
            @if($rid == 4 )
                <h1>资产发币配置管理</h1>
                <small>{{ trans('page.total', ['total'=> $total = $coins->total()]) }}</small>
            @elseif($rid == 3)
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">操作员审核列表</h3>
                        <small>{{ trans('page.total', ['total'=> $total = $users->total()]) }}</small>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th class="visible-lg">员工序号</th>
                                <th class="">角色</th>
                                <th>用户名</th>
                                <th>状态</th>
                                <th>审核</th>
                            </tr>
                            @foreach($users as $user)
                                @foreach($user->roles as $rkey => $role)
                                    @if($role->id == 4)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>
                                                @foreach($user->roles as $rkey => $role)
                                                    @if ($rkey > 0 ) , @endif
                                                    {{ $role->display_name }}
                                                @endforeach
                                            </td>
                                            <td>
                                                {{--<td>{{ $user->username }}</td>--}}
                                                {{ substr($user->email,0,strripos($user->email,"@")).'@'.md5(substr($user->email,strripos($user->email,"@")+1))}}
                                            </td>
                                            <td>
                                                @if($user->status == 1)
                                                    注册申请已通过
                                                @elseif($user->status == 2)
                                                    注册申请未通过
                                                @elseif($user->status == 0)
                                                    注册申请中
                                                @elseif($user->status == 3)
                                                    申请重置密码
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->status == 1 )
                                                    已通过
                                                @elseif($user->status == 0)
                                                    <a href="{{ url('coin/update2', ['id' => $user->id,'item'=>1 ]) }}">通过</a>
                                                    <a href="{{ url('coin/update2', ['id' => $user->id,'item'=>0 ]) }}">不通过</a>
                                                @elseif($user->status == 2)
                                                    未通过
                                                    {{--<a href="#" class="delete" data-name="{{ $user->email }}" data-url="{{ url('admin/user/delete') }}" data-id="{{ $user->id }}" data-toggle="modal" data-target="#doDelete">通过</a>--}}
                                                @elseif($user->status == 3)
                                                    <a href="{{ url('coin/update2', ['id' => $user->id,'item'=>2 ]) }}">同意</a>
                                                    <a href="{{ url('coin/update2', ['id' => $user->id,'item'=>3 ]) }}">不同意</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-left">
                            <li><a href="#"> {{ $users->currentPage() }} / {{ $users->lastPage() ?: 1 }}</a></li>
                        </ul>
                        @if ($users->lastPage() != 1)
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <li><a href="{{ $users->url(1) }}">{{ trans('common.page.first') }}</a></li>
                                <li><a href="{{ $users->previousPageUrl() }}">{{ trans('common.page.prev') }}</a></li>
                                <li><a href="{{ $users->nextPageUrl() }}">{{ trans('common.page.next') }}</a></li>
                                <li><a href="{{ $users->url($users->lastPage()) }}">{{ trans('common.page.last') }}</a></li>
                            </ul>
                        @endif
                    </div>
                </div>
                <h1>发币审核管理</h1>
                <small>{{ trans('page.total', ['total'=> $total = $coins->total()]) }}</small>
            @elseif($rid == 0)
                {{--您还没有被分配角色--}}
            @endif
    </section>

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('common.search_box') }}</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <form action="{{ url('user') }}" method="get">
                    <div class="col-lg-2">
                        <input name="email" value="{{ Input::get('capitalists', '') }}" type="text" class="form-control" placeholder="资产方" />
                    </div>
                    <div class="col-lg-2">
                        <input name="status" value="{{ Input::get('c_status', '') }}" type="text" class="form-control" placeholder="审核状态" />
                    </div>
                    <div class="col-lg-2">
                        <button type="submit" class="btn btn-default col-md-6">{{ trans('common.search') }}</button>
                        <button type="button" class="btn btn-default col-md-6" onclick="location.href='{{ url('user') }}';">{{ trans('common.reset') }}</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    @if($rid == 4)
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- /.box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">资产发币配置列表</h3>
                            <span class="pull-right"><button type="button" onclick="location='{{ url('coin/add') }}';" class="btn btn-success pull-right">添加</button></span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="visible-lg">编号</th>
                                    <th class="">资产方</th>
                                    <th>币种</th>
                                    <th>发币量</th>
                                    <th>单位</th>
                                    <th>交割最小单位</th>
                                    <th>详情</th>
                                    <th>配置时间</th>
                                    <th>审核</th>
                                    <th>编辑</th>
                                </tr>
                                @foreach($coins as $coin)
                                    <tr>
                                        <td>{{ $coin->id }}</td>
                                        <td>{{ $coin->capitalists }}</td>
                                        <td>{{ $coin->coin_type }}</td>
                                        <td>{{ $coin->coin_num }}</td>
                                        <td>{{ $coin->coin_unit }}</td>
                                        <td>{{ $coin->coin_unit_min }}</td>
                                        <td>{{ $coin->description }}</td>
                                        <td>{{ $coin->created_at }}</td>
                                        <td>
                                            @if($coin->c_status == 1)
                                                审核已通过
                                            @elseif($coin->c_status == 0)
                                                审核未通过
                                            @elseif($coin->c_status == 2)
                                                待审核
                                            @endif
                                        </td>
                                        <td>
                                            @if($coin->c_status == 1)
                                                审核已通过，不可操作
                                            @elseif($coin->c_status == 0)
                                                <a href="{{ url('coin/edit', ['id' => $coin->id]) }}">编辑</a>
                                            @elseif($coin->c_status == 2)
                                                待审核,暂时不可操作
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-left">
                                <li><a href="#"> {{ $coins->currentPage() }} / {{ $coins->lastPage() ?: 1 }}</a></li>
                            </ul>
                            @if ($coins->lastPage() != 1)
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li><a href="{{ $coins->url(1) }}">{{ trans('common.page.first') }}</a></li>
                                    <li><a href="{{ $coins->previousPageUrl() }}">{{ trans('common.page.prev') }}</a></li>
                                    <li><a href="{{ $coins->nextPageUrl() }}">{{ trans('common.page.next') }}</a></li>
                                    <li><a href="{{ $coins->url($coins->lastPage()) }}">{{ trans('common.page.last') }}</a></li>
                                </ul>
                            @endif
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    @elseif($rid == 3)
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- /.box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">发币审核列表</h3>
                         </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="visible-lg">编号</th>
                                    <th class="">资产方</th>
                                    <th>币种</th>
                                    <th>发币量</th>
                                    <th>单位</th>
                                    <th>交割最小单位</th>
                                    <th>详情</th>
                                    <th>配置时间</th>
                                    <th>审核</th>
                                </tr>
                                @foreach($coins as $coin)
                                    <tr>
                                        <td>{{ $coin->id }}</td>
                                        <td>{{ $coin->capitalists }}</td>
                                        <td>{{ $coin->coin_type }}</td>
                                        <td>{{ $coin->coin_num }}</td>
                                        <td>{{ $coin->coin_unit }}</td>
                                        <td>{{ $coin->coin_unit_min }}</td>
                                        <td>{{ $coin->description }}</td>
                                        <td>{{ $coin->created_at }}</td>
                                        <td>
                                            @if($coin->c_status == 1)
                                                审核已通过
                                            @elseif($coin->c_status == 0)
                                                审核未通过
                                            @elseif($coin->c_status == 2)
                                                <a href="{{ url('coin/update', ['id' => $coin->id,'item'=>1 ]) }}">通过</a>
                                                <a href="{{ url('coin/update', ['id' => $coin->id,'item'=>0 ]) }}">不通过</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-left">
                                <li><a href="#"> {{ $coins->currentPage() }} / {{ $coins->lastPage() ?: 1 }}</a></li>
                            </ul>
                            @if ($coins->lastPage() != 1)
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li><a href="{{ $coins->url(1) }}">{{ trans('common.page.first') }}</a></li>
                                    <li><a href="{{ $coins->previousPageUrl() }}">{{ trans('common.page.prev') }}</a></li>
                                    <li><a href="{{ $coins->nextPageUrl() }}">{{ trans('common.page.next') }}</a></li>
                                    <li><a href="{{ $coins->url($coins->lastPage()) }}">{{ trans('common.page.last') }}</a></li>
                                </ul>
                            @endif
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    @elseif($rid == 0)
        <section>
            <h1>
                您还没有被分配角色，请等管理员分配角色后再进行操作！！
            </h1>
        </section>
    @endif
@endsection