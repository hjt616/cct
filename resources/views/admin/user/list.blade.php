@extends('layouts.admin')

@section('title') {{ trans('admin.menu_list.user') }} @endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ trans('admin.menu_list.user') }}
            <small>{{ trans('page.total', ['total'=> $total = $users->total()]) }}</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('common.search_box') }}</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <form action="{{ url('/admin/user/list') }}" method="get">
                                <div class="col-lg-2">
                                    <input name="email" value="{{ Input::get('email', '') }}" type="text" class="form-control" placeholder="用户名" />
                                </div>
                                <div class="col-lg-2">
                                     <input name="status" value="{{ Input::get('status', '') }}" type="text" class="form-control" placeholder="注册状态" />
                                </div>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn btn-default col-md-6">{{ trans('common.search') }}</button>
                                    <button type="button" class="btn btn-default col-md-6" onclick="location.href='{{ url('admin/user/list') }}';">{{ trans('common.reset') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('admin.menu_list.user_list') }}</h3>
                     </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <th class="visible-lg">员工序号</th>
                                <th class="">角色</th>
                                <th>用户名</th>
                                <th>状态</th>
                                <th>审核</th>
                            </tr>
                            @foreach($users as $user)
                                @foreach($user->roles as $rkey => $role)
                                    @if($role->id == 3)
                                        <tr>
                                            <td>
                                                {{ $user->id }}
                                            </td>
                                            <td>
                                                @foreach($user->roles as $rkey => $role)
                                                    @if ($rkey > 0 ) , @endif
                                                        {{ $role->display_name }}
                                                @endforeach
                                            </td>
                                            <td>
                                                {{ substr($user->email,0,strripos($user->email,"@")).'@'.md5(substr($user->email,strripos($user->email,"@")+1))}}
                                            </td>
                                            <td>
                                                @if($user->status == 1)
                                                    注册申请已通过
                                                @elseif($user->status == 2)
                                                    注册申请未通过
                                                @elseif($user->status == 0)
                                                    注册申请中
                                                @elseif($user->status == 3)
                                                    申请重置密码
                                                @endif
                                            </td>
                                            <td>
                                                @if($user->status == 1 )
                                                    已通过
                                                @elseif($user->status == 0)
                                                    <a href="{{ url('admin/user/update', ['id' => $user->id,'item'=>1 ]) }}">通过</a>
                                                    <a href="{{ url('admin/user/update', ['id' => $user->id,'item'=>0 ]) }}">不通过</a>
                                                @elseif($user->status == 2)
                                                    未通过
                                                    {{--<a href="#" class="delete" data-name="{{ $user->email }}" data-url="{{ url('admin/user/delete') }}" data-id="{{ $user->id }}" data-toggle="modal" data-target="#doDelete">通过</a>--}}
                                                @elseif($user->status == 3 )
                                                    <a href="{{ url('admin/user/update', ['id' => $user->id,'item'=>2 ]) }}">同意</a>
                                                    <a href="{{ url('admin/user/update', ['id' => $user->id,'item'=>3 ]) }}">不同意</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-left">
                            <li><a href="#"> {{ $users->currentPage() }} / {{ $users->lastPage() ?: 1 }}</a></li>
                        </ul>
                        @if ($users->lastPage() != 1)
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="{{ $users->url(1) }}">{{ trans('common.page.first') }}</a></li>
                            <li><a href="{{ $users->previousPageUrl() }}">{{ trans('common.page.prev') }}</a></li>
                            <li><a href="{{ $users->nextPageUrl() }}">{{ trans('common.page.next') }}</a></li>
                            <li><a href="{{ $users->url($users->lastPage()) }}">{{ trans('common.page.last') }}</a></li>
                        </ul>
                        @endif
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection
