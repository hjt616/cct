@extends('layouts.admin')

@section('title') {{ trans('admin.menu_list.user') }} @endsection

@section('css')
<link rel="stylesheet" href="{{ Setting::get('cdn_url', '/') }}plugins/select2/select2.min.css">
@endsection

@section('content')

    <section class="content-header">
        <h1>
            {{ trans('admin.menu_list.user') }}
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @include('public/message')
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                @if (isset($item))
                <h3 class="box-title">
                    角色分配
                    <small>User Id: {{ $item->id }}</small>
                </h3>
                @else
                    <h3 class="box-title">{{ trans('admin.menu_list.user_add') }}</h3>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ isset($item) ? url('admin/user/edit') :  url('admin/user/add') }}" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label for="role" class="col-sm-2 control-label">{{ trans('common.role') }}</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="roles[]" title="role" multiple="multiple" id="role-select">
                                @foreach($roles as $role)
                                    <option @if(isset($item)) @foreach($item->roles as $uRole) @if($role->id == $uRole->id) selected @endif @endforeach @endif value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="status" name="status"
                                   value="{{ isset($item) ? $item->status : old('status') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">{{ trans('common.description') }}</label>
                        <div class="col-sm-10">
                            <textarea id="description" title="description" name="description" class="form-control">{{ isset($item) ?  $item->profile->description : old('description') }}</textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-cancel pull-left" onclick="location='{{ url('admin/user/list') }}';">{{ trans('common.return') }}</button>
                    @if (isset($item))
                        <input type="hidden" name="id" value="{{ $item->id }}" />
                        <button type="submit" class="btn btn-info pull-right">{{ trans('common.edit') }}</button>
                    @else
                        <button type="submit" class="btn btn-success pull-right">{{ trans('common.add') }}</button>
                    @endif
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </section>

@endsection

@section('js')
<script type="text/javascript" src="{{ Setting::get('cdn_url', '/') }}plugins/AjaxFileUpload/jquery.ajaxfileupload.js"></script>
<script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/select2.full.min.js"></script>
<script src="{{ Setting::get('cdn_url', '/') }}plugins/select2/i18n/zh-CN.js"></script>
<script>
    $(function(){
        $('#avatar').AjaxFileUpload({
            action: $('#avatar').attr('data-url'),
            onComplete: function (filename, response) {
                if (response.status == 'success') {
                    $('#avatar-img').attr('src', '/' + response.path + '?t=' + Math.random());
                    $('#avatar-input').val(response.path);
                } else {
                    alert(response.msg);
                }
            }
        });

        $('select').select2({
            placeholder: "{{ trans('common.role_select') }}",
            language: "zh-CN"
        });
    });

    function getRegion(_nextId, _pid, _type, _defalutId) {
        var $region = $('#'+_nextId);
        $region.html('<option value="0" selected>{{ trans('common.select') }}</option>');
        $.get('/user/region?type='+_type+'&pid='+_pid,function(data){
            if (data.status == 0) {
                var _list = data.list;
                if ($(_list).first().length != 0) {
                    $region.show();
                    $.each(_list, function(i,val){
                        $region.append('<option '+( val.id == _defalutId ? 'selected' : '' )+' value="'+val.id+'">'+val.name+'</option>');
                        $region.trigger('change.select2');
                    });
                } else {
                    $region.hide();
                }
            }
        });
    }
</script>
@endsection
