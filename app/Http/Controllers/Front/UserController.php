<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Common\RegionController;
use App\Http\Controllers\Common\AvatarController;
use App\Models\User;
use App\Models\Coin;
use Illuminate\Http\Request;
use Input;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseAuthController
{
    use RegionController, AvatarController;

    public function getIndex()
    {
        $users = User::query()->leftjoin('user_profiles as up', 'up.user_id', '=', 'users.id')->leftjoin('role_user as ro', 'ro.user_id', '=', 'users.id')->where('ro.role_id',4);
        $inputs = Input::all();
        foreach ($inputs as $inputKey => $inputValue) {
            if ($inputValue !== '' && $inputKey != 'page') {
                $users = $users->where($inputKey, 'LIKE', '%' . $inputValue . '%');
            }
        }
        $users = $users->orderBy('id', 'DESC')->paginate(10);

//        $coins = \DB::table('coin')->get();//获取资产发行配置的数据
        $coins = Coin::query();
        $coins = $coins->orderBy('id', 'DESC')->paginate(10);

        $id = Auth::id();//员工id
        $status = \DB::table('users')->where('id', $id)->select('status')->first();
        $statu = $status->status;//员工状态

        if($statu == 0){
            //账户注册审核中
            return $this->render('user.sign');
        }elseif($statu == 3){
            //密码重置
            return $this->render('user.register');
        }elseif($statu == 2){
            //账户注册审核未通过
            return $this->render('user.check');
        }elseif($statu == 1){
            //查找角色id
            $roid = \DB::table('role_user')->where('user_id', $id)->select('role_id')->first();

            if(isset($roid)){
                $rid = $roid->role_id;
            }else{
                $rid = 0;
            }

            return $this->render('user.index', compact('users','rid','coins','statu'));
        }

    }

    public function getProfile()
    {
        $this->getRegionProvinces();
        $item = $this->authUser;
        return $this->render('user.profile', ['item' => $item]);
    }

    public function postProfile(Request $request)
    {
        $this->validate($request, [
            'status' => 'max:1000',
            'nickname' => 'max:255',
            'description' => 'max:1000',
            'password' => 'confirmed|min:6',

        ]);
        /** @var User $user */
        $user = User::find($this->authUser['id']);
        $password = Input::get('password', '');
        $role = Input::get('role', '修改密码');
        if (!empty($password)) {
            $user->password = bcrypt($password);
        }
        if ($user->save()) {
            $data = Input::all();
            $data['role'] = $role;
            $user->saveProfile($data);
            return $this->successReturn(trans("common.edit_success"));
        }
        return $this->errorReturn(trans("common.edit_fail"));
    }


}
