<?php
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Common\RegionController;
use App\Http\Controllers\Common\AvatarController;
use App\Models\Coin;
use Illuminate\Http\Request;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Models\User;



class  CoinController extends BaseAuthController
{
    use RegionController, AvatarController;

    public function getIndex()
    {
        return $this->render('coin.index');
    }

    public function getAdd()
    {

//        return $this->render('user.item');
        return $this->render('coin.index');
    }

    public function postAdd(Request $request)
    {
        $coin = new Coin();
//        $this->validate($request, [
//            'username' => 'required',
//            'email' => 'required|email',
//            'password' => 'required|confirmed|min:6',
//        ]);
        $coin->capitalists = Input::get('capitalists');
        $coin->coin_type = Input::get('coin_type');
        $coin->coin_num = Input::get('coin_num');
        $coin->coin_unit = Input::get('coin_unit');
        $coin->coin_unit_min = Input::get('coin_unit_min');
        $coin->description = Input::get('description');
        $coin->c_status = Input::get('c_status','2');//审核状态默认为待审核
        return $this->saveCoin($coin, $request);
    }

    public function getEdit($id)
    {
//        $this->getAllRoles();
//        $this->getRegionProvinces();
//        return $this->getBaseItem('user', $id, 'user.item');
        \DB::table('coin')->where('id', $id)->update(['c_status' => 2]);//编辑配置方案是自动将审核状态变更为‘2’，待审核

        return $this->getBaseItem('coin', $id, 'coin.index');
    }

    public function postEdit(Request $request)
    {
        $coin = $this->getBaseItem('coin', Input::get('id'));

        $coin->capitalists = Input::get('capitalists');
        $coin->coin_type = Input::get('coin_type');
        $coin->coin_num = Input::get('coin_num');
        $coin->coin_unit = Input::get('coin_unit');
        $coin->coin_unit_min = Input::get('coin_unit_min');
        $coin->description = Input::get('description');

        return $this->savecoin($coin, $request);
    }

    protected function saveCoin($coin, Request $request)
    {
        $this->validate($request, [

            'description' => 'max:1000',

        ]);
//        $password = Input::get('password', '');
//        if (!empty($password)) {
//            $user->password = bcrypt($password);
//        }
//        $user->mobile = Input::get('mobile', '');
        $isNew = $coin->isNew();
        $type = $isNew ? 'add' : 'edit';
        if ($coin->save()) {

            return $this->successReturn(trans("common.{$type}_success"));
        }
        return $this->errorReturn(trans("common.{$type}_fail"));
    }

    public function getUpdate($id,$item)
    {

        $users = User::query()->leftjoin('user_profiles as up', 'up.user_id', '=', 'users.id')->leftjoin('role_user as ro', 'ro.user_id', '=', 'users.id')->where('ro.role_id',4);
        $inputs = Input::all();
        foreach ($inputs as $inputKey => $inputValue) {
            if ($inputValue !== '' && $inputKey != 'page') {
                $users = $users->where($inputKey, 'LIKE', '%' . $inputValue . '%');
            }
        }
        $users = $users->orderBy('id', 'DESC')->paginate(10);

        if($item == 1){
            \DB::table('coin')->where('id', $id)->update(['c_status' => 1]);
        }elseif($item == 0){
            \DB::table('coin')->where('id', $id)->update(['c_status' => 0]);
        }

        $id = Auth::id();//员工id
        $coins = Coin::query();
        $coins = $coins->orderBy('id', 'DESC')->paginate(10);

        //查找角色id
        $roid = \DB::table('role_user')->where('user_id', $id)->select('role_id')->first();

        if(isset($roid)){
            $rid = $roid->role_id;
        }else{
            $rid = 0;
        }

        return $this->render('user.index', compact('users','rid','coins'));
    }

    public function getUpdate2($id,$item)
    {
        $users = User::query()->leftjoin('user_profiles as up', 'up.user_id', '=', 'users.id')->leftjoin('role_user as ro', 'ro.user_id', '=', 'users.id')->where('ro.role_id',4);
        $inputs = Input::all();
        foreach ($inputs as $inputKey => $inputValue) {
            if ($inputValue !== '' && $inputKey != 'page') {
                $users = $users->where($inputKey, 'LIKE', '%' . $inputValue . '%');
            }
        }
        $users = $users->orderBy('id', 'DESC')->paginate(10);

        if($item == 1){
            \DB::table('users')->where('id', $id)->update(['status' => 1]);
        }elseif($item == 0){
            \DB::table('users')->where('id', $id)->update(['status' => 2]);
        }elseif($item == 2){
            $password= bcrypt('000000');
            \DB::table('users')->where('id', $id)->update(['password' => $password]);
            \DB::table('users')->where('id', $id)->update(['status' => 1]);
        }elseif($item == 3){
            \DB::table('users')->where('id', $id)->update(['status' => 2]);
        }

        $id = Auth::id();//员工id
        $coins = Coin::query();
        $coins = $coins->orderBy('id', 'DESC')->paginate(10);

        //查找角色id
        $roid = \DB::table('role_user')->where('user_id', $id)->select('role_id')->first();

        if(isset($roid)){
            $rid = $roid->role_id;
        }else{
            $rid = 0;
        }

        return $this->render('user.index', compact('users','rid','coins'));
    }


    public function postUpdate($id)
    {
        return $this->getBaseItem('coin', $id, 'coin.index');
    }


}
