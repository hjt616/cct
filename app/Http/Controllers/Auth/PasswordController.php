<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Input;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $inputs = Input::all();
        if($inputs){
            $email = $inputs['email'];
            //当员工发出忘记密码申请修改密码时，将员工状态改为申请密码重置，向管理员发出申请
            \DB::table('users')->where('email', $email)->update(['status' => 3]);
            return $this->successReturn('已发出重置密码申请，待审核！！');
        }

        $this->middleware('guest');
        $this->subject = trans('auth.reset_email_subject');
    }
}
